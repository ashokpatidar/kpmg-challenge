# 3 Tier architecture diagram with Auto Scalling group & managed Database, refer to 3 tier_Diagram_Sample.jpg

Presentation tier or Web tier (Public Exposed with Loadbalancer)
Logic tier or Application Tier (with Private subnet )
Data or database tier (with Private subnet )

##### Terraform Script's  ##### 
The script will deploy a load balancer with autoscaling and an Ubuntu server with Apache in a private subnet, without a public IP, inside a managed instance group. Additionally, it will deploy a Managed MySQL database using Terraform.

provider.tf --> Configure Google Cloud provider

lb-managed.tf --> Create managed instance group, backend services and all components required by the load balancer 

lb-managed-variables.tf --> Load balancer variables

network-firewall.tf --> Configure basic firewall for the network

network-variables.tf --> Define network variables

network.tf --> Define network, vpc, subnet, icmp firewall

database.tf --> Define basic setup of MySQL managed database with local IP 

database-variables.tf --> Define database variables

terraform.tfvars --> Defining variables 

variables-auth.tf --> Application and authentication variables

vm.tf --> Template to create a Ubuntu VM with Apache web server