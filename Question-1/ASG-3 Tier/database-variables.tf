# database  variables 


# define GCP region
variable "db_user" {
  type = string
  description = "mysql"
}

variable "deletion_protection_enabled" {
  description = "Deletion protection"
  type        = bool
  default     = true
}


# define Password
variable "db_password" {
  type = string
  description = "mysql"
}
