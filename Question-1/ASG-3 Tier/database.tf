
# MySql managed Database with basic settings 
resource "google_compute_global_address" "private_ip_address" {
  name           = "private-ip-address"
  purpose        = "VPC_PEERING"
  address_type   = "INTERNAL"
  prefix_length  = 16
  #network        = google_compute_network.private_network.name
  network        = google_compute_network.vpc.name
}

resource "google_service_networking_connection" "private_vpc_connection" {
  #network                = google_compute_network.private_network.name
  network                = google_compute_network.vpc.name
  service                = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "google_sql_database_instance" "instance" {
  name               = "private-instance"
  region             = var.gcp_region_1
  database_version   = "MYSQL_5_7"
  depends_on         = [google_service_networking_connection.private_vpc_connection]

  settings {
    tier = "db-f1-micro"
	deletion_protection_enabled = var.deletion_protection_enabled
    ip_configuration {
      ipv4_enabled    = false
      #private_network = google_compute_network.private_network.self_link
	  private_network = google_compute_network.vpc.self_link
    }
  } 
}


# Create a database user
resource "google_sql_user" "my_user" {
  name     = var.db_user
  password = var.db_password
  instance = google_sql_database_instance.instance.name
  host     = "%"
  depends_on = [google_sql_database_instance.instance]
}
