# Application Definition 
app_name        = "myapp" #do NOT enter any spaces
app_environment = "test" # Dev, Test, Prod, etc
app_domain      = "abc.com"
app_project     = "gcp_project_id"
app_node_count  = 1

# GCP Settings
gcp_region_1  = "europe-west1"
gcp_zone_1    = "europe-west1-b"
gcp_auth_file = "C:/Users/abc/OneDrive/Desktop/KPMG/Standerd/nexval-infotech.json"

# GCP Netwok
private_subnet_cidr_1  = "10.10.1.0/24"


# Database Setting 
region     = "europe-west1"
db_name    = "my-database"
db_user    = "my-user"
db_password = "my-password"