# 3 Tier simple architecture diagram, refer to 3 tier_Diagram_Sample.jpg

Presentation tier or Web tier (Public Exposed with External IP )
Logic tier or Application Tier (with Private subnet )
Data or database tier (with Private subnet )

##### Terraform Script's  ##### 
The script will deploy 3 compute engine instances 
1. The Presentation Tier, also known as the Web Tier, is considered the front-end and is publicly exposed with an External IP. It utilizes Apache as the web server.
2. The Logic Tier, also known as the Application Tier, is considered the back-end and is accessed only by the VPC using an internal IP. It utilizes Nginx as the web server.
3. The Data Tier, also known as the Database Tier, is considered the database back-end and is accessed only by the VPC using an internal IP. It utilizes MySQL as the database server.

main.tf --> complete terraform script 