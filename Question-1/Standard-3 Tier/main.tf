provider "google" {
  credentials = file("GCP_CREDENTIALS_FILE.json")
  project     = "GCP_PROJECT_ID"
  region      = "us-central1"
}

resource "google_compute_network" "network" {
  name                    = "org-vpc-network"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "frontend" {
  name          = "public-web-tier"
  ip_cidr_range = "192.168.201.0/26"
  network       = google_compute_network.network.self_link
  region        = "us-central1"
}

resource "google_compute_subnetwork" "application" {
  name          = "private-backend-tier"
  ip_cidr_range = "192.168.201.64/26"
  network       = google_compute_network.network.self_link
  region        = "us-central1"
}

resource "google_compute_subnetwork" "database" {
  name          = "private-database-tier"
  ip_cidr_range = "192.168.201.128/26"
  network       = google_compute_network.network.self_link
  region        = "us-central1"
}

resource "google_compute_firewall" "allow_http" {
  name    = "allow-http"
  network = google_compute_network.network.self_link

  allow {
    protocol = "tcp"
    ports    =  ["80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["frontend-tier"]
}
resource "google_compute_firewall" "allow_internal" {
  name    = "allow-internal"
  network = google_compute_network.network.self_link

  allow {
    protocol = "tcp"
    ports    =  ["3306", "443", "22", "80"]
  }

  source_ranges = ["192.168.201.0/26"]
  target_tags   = ["application-tier", "database-tier"]
}

resource "google_compute_instance" "frontend" {
  name         = "frontend-instance"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.frontend.self_link
    access_config {}
  }

  metadata_startup_script = "sudo apt-get update && sudo apt-get install -y apache2 && sudo systemctl start apache2 && sudo systemctl enable apache2"
  
  tags = ["frontend-tier"]
}

resource "google_compute_instance" "application" {
  name         = "application-instance"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.application.self_link
  }

  metadata_startup_script = "sudo apt-get update && sudo apt-get install -y nginx && sudo systemctl start nginx && sudo systemctl enable nginx"
  tags = ["application-tier"]
}

resource "google_compute_instance" "database" {
  name         = "database-instance"
  machine_type = "f1-micro"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.database.self_link
  }

  metadata_startup_script = "sudo apt-get update && sudo apt-get install -y mysql-server"
   tags = ["database-tier"]
}
