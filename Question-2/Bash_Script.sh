#!/bin/bash
instance_id=COMPUTE_ENGINE_INSTANCEID
zone=asia-south1-c
network_ip=$(gcloud compute instances describe $instance_id --zone=$zone --format=json | grep networkIP | awk '{print $2}' | tr -d '",')
echo "Network IP: $network_ip"