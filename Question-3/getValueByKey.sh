#!/bin/bash

function get_value() {
    local key=$1
    shift
    local obj=("$@")
    local IFS='/'
    local keys=($key)
    local val=${obj[@]}
    for k in "${keys[@]}"; do
        val=${val[$k]}
    done
    echo $val
}

# Example Input
obj1='{ "a": { "b": { "c": "d" } } }'
key1='a/b/c'

obj2='{ "x": { "y": { "z": "a" } } }'
key2='x/y/z'

# Convert object strings to associative arrays
declare -A obj1_arr=$(echo "$obj1" | jq -r '.')
declare -A obj2_arr=$(echo "$obj2" | jq -r '.')

# Call the get_value function for each example input
echo "Value 1: $(get_value $key1 "${obj1_arr[@]}")"
echo "Value 2: $(get_value $key2 "${obj2_arr[@]}")"
